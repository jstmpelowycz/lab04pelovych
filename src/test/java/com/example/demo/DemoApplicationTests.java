package com.example.demo;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

public class DemoApplicationTests {
	private static final String DEF_PROP = "DEF_PROP";
	private static final String EMPTY_PROP = "";

	@Test
	public void run() {
		DemoApplication firstApp = new DemoApplication(DemoApplicationTests.DEF_PROP);
		DemoApplication secondApp = new DemoApplication(DemoApplicationTests.EMPTY_PROP);

		assertThat(firstApp.getProperty().length(), is(not(0)));
		assertThat(secondApp.getProperty().length(), is(0));
	}
}
