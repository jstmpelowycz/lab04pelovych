package com.example.demo;

public class DemoApplication {
	private final String property;

	public DemoApplication(String property) {
		this.property = property;
	}

	public String getProperty() {
		return this.property;
	}
}
